#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#define MKL_ILP64
#include <mkl.h>
#include <aio.h>


#define TRANSFER_MAX 2147479552
#define ELEM_TRMAX 268434944

void asyncWrite(const int fd, const unsigned long iniPos, double* buff, const unsigned long nBytes, struct aiocb* control);

int main(int argc, const char* argv[])
{
	int i, j,k,x,y, info, fd, nControlW;
	unsigned long n,nB,tB,pos, bytesB,bytesL,bytesU;
	FILE* f;
	double ini,fin,t;
	const char* fname;
	double* B,*Aij,* Aii,* Aik, *Ajk,* Ajj,* S;
	struct aiocb* controlW;
	
	//timers
	int nEsc = 0,nLect = 0,nDgemm = 0, nDtrsm = 0,nChol = 0, nForm = 0, nResta = 0;
	double tEsc = 0,tLect = 0,tDgemm = 0, tDtrsm = 0,tChol = 0, tForm = 0, tResta = 0;
	double t1, t2;
	
	if(argc != 2)
	{
		printf("Uso: cholesky problem_file");
		exit(1);
	}
	else
	{
		fname = argv[1];
	}
	
	

	
	
	ini = dsecnd();
	//Parameter reading
	f = fopen(fname,"rb+");
	fread(&n,sizeof(unsigned long),1,f);
	fread(&tB,sizeof(unsigned long),1,f);
	fd = fileno(f);
	
	nB = n/ tB;
	bytesB = tB*tB*sizeof(double);
	bytesL = tB*(tB+1)/2*sizeof(double);
	bytesU = tB*(tB-1)/2*sizeof(double);
	printf("n = %d, tB = %d, nB = %d\n",n,tB,nB);
	
	//Memory preallocation
	S = (double*)mkl_malloc(bytesB,64);
	Aik = (double*)mkl_malloc(bytesB,64);
	Ajk = (double*)mkl_malloc(bytesB,64);
	Aij = Aik;
	Aii = Aik;
	B = Ajk;
	Ajj = (double*)mkl_malloc(bytesB,64);
	
 	//Async write handlers init
	nControlW = 1 +((bytesB - 1)/TRANSFER_MAX);
	controlW = (struct aiocb*)mkl_malloc( nControlW * sizeof(struct aiocb),64);
	for(i = 0; i < nControlW; i++)
	{
			controlW[i].aio_sigevent.sigev_notify= SIGEV_NONE;
			controlW[i].aio_reqprio = 0;
	}	
	
	//First column doesn't need anything from previous columns
	j = 0;
	
	t1 = dsecnd();
	fread(Aii,sizeof(double),tB*(tB+1)/2,f);
	t2 = dsecnd();
	tLect += (t2 - t1);
	nLect++;
	
	t1 = dsecnd();
	info = LAPACKE_dpftrf(LAPACK_COL_MAJOR,'N','L',tB,Aii); 
	if (info != 0)
		printf("Error en lapack(dpftrf): %d\n",info);
	t2 = dsecnd();
	tChol += (t2 - t1);
	nChol++;
	
	t1 = dsecnd();
	asyncWrite(fd,16,Aii,bytesL, controlW);
	t2 = dsecnd();
	tEsc += (t2 - t1);
	nEsc++;
	
	t1 = dsecnd();
	info = LAPACKE_dtfttr(LAPACK_COL_MAJOR,'N','L',tB,Aii,Ajj,tB);
	if (info != 0)
		printf("Error en lapack(dpftrs): %d\n",info);
	t2 = dsecnd();
		tForm += (t2 - t1);
		nForm++;
	
	for(i = 1; i < nB; i++)
	{
		t1 = dsecnd();
		pos = (i+j*nB)*bytesB-(j+1)*bytesU+16;
		fseek(f,pos,SEEK_SET);
		fread(Aij,sizeof(double),tB*tB,f);
		t2 = dsecnd();
		tLect += (t2 - t1);
		nLect++;
				
		t1 = dsecnd();
		cblas_dtrsm(CblasColMajor,CblasRight,CblasLower,CblasTrans,CblasNonUnit,tB,tB,1,Ajj,tB,Aij,tB);
		t2 = dsecnd();
		tDtrsm += (t2 - t1);
		nDtrsm++;
		
		t1 = dsecnd();
		asyncWrite(fd,pos,Aij,bytesB, controlW);
		t2 = dsecnd();
		tEsc += (t2 - t1);
		nEsc++;
	}
		
	//remaining columns
	for(j = 1; j < nB; j++)
	{
		for(i = j; i < nB; i++)
		{
			if( i == j)
			{
			
				cblas_dscal(tB*tB,0,S,1);//init S to 0 
				for(k = 0; k < j;k++)
				{
					t1 = dsecnd();
					fseek(f,(i+k*nB-(k*(k+1)/2))*bytesB-(k+1)*bytesU+16,SEEK_SET);
					fread(Aik,sizeof(double),tB*tB,f);
					t2 = dsecnd();
					tLect += (t2 - t1);
					nLect++;
					
					t1 = dsecnd();
					cblas_dsyrk(CblasColMajor,CblasLower,CblasNoTrans,tB,tB,1,Aik,tB,1,S,tB);
					t2 = dsecnd();
					tDgemm += (t2 - t1);
					nDgemm++;
				}
				pos = (i+j*nB-(j*(j+1)/2))*bytesB-(j)*bytesU+16;
				t1 = dsecnd();
				fseek(f,pos,SEEK_SET);
				fread(Aii,sizeof(double),tB*(tB+1)/2,f);
				t2 = dsecnd();
				tLect += (t2 - t1);
				nLect++;
				
				//substraction Aii = Aii - S packing S to RFPStorage
				t1 = dsecnd();
				LAPACKE_dtrttf(LAPACK_COL_MAJOR,'N','L',tB,S,tB,B);
				t2 = dsecnd();
				tForm += (t2 - t1);
				nForm++;
				
				t1 = dsecnd();
				cblas_daxpy(tB*(tB+1)/2,-1,B,1,Aii,1);
				t2 = dsecnd();
				tResta += (t2 - t1);
				nResta++;

				//Cholesky Decomposition
				t1 = dsecnd();
				info = LAPACKE_dpftrf(LAPACK_COL_MAJOR,'N','L',tB,Aii); 
				if (info != 0)
					printf("[%d,%d]Error en lapack(dpftrf): %d\n",i,j,info);
				t2 = dsecnd();
				tChol += (t2 - t1);
				nChol++;
				
				t1 = dsecnd();
				asyncWrite(fd,pos,Aii,bytesL, controlW);
				t2 = dsecnd();
				tEsc += (t2 - t1);
				nEsc++;
				
				//Unpacking Aii from RFP storage to full storage
				t1 = dsecnd();
				info = LAPACKE_dtfttr(LAPACK_COL_MAJOR,'N','L',tB,Aii,Ajj,tB);
				t2 = dsecnd();
				tForm += (t2 - t1);
				nForm++;
			}
			else
			{
			
				cblas_dscal(tB*tB,0,S,1);//init S to 0 
				for(k = 0; k < j;k++)
				{
					t1 = dsecnd();
					fseek(f,(i+k*nB-(k*(k+1)/2))*bytesB-(k+1)*bytesU+16,SEEK_SET);
					fread(Aik,sizeof(double),tB*tB,f);
					
					fseek(f,(j+k*nB-(k*(k+1)/2))*bytesB-(k+1)*bytesU+16,SEEK_SET);
					fread(Ajk,sizeof(double),tB*tB,f);
					t2 = dsecnd();
					tLect += (t2 - t1);
					nLect+=2;
					
					t1 = dsecnd();
					cblas_dgemm(CblasColMajor,CblasNoTrans,CblasTrans,tB,tB,tB,1,Aik,tB,Ajk,tB,1,S,tB);	
					t2 = dsecnd();
					tDgemm += (t2 - t1);
					nDgemm++;
				}
				pos = (i+j*nB-(j*(j+1)/2))*bytesB-(j+1)*bytesU+16;
				t1 = dsecnd();
				fseek(f,pos,SEEK_SET);
				fread(Aij,sizeof(double),tB*tB,f);
				t2 = dsecnd();
				tLect += (t2 - t1);
				nLect++;
				
				t1 = dsecnd();
				cblas_daxpy(tB*tB,-1,S,1,Aij,1);
				t2 = dsecnd();
				tResta += (t2 - t1);
				nResta++;
				
				t1 = dsecnd();
				cblas_dtrsm(CblasColMajor,CblasRight,CblasLower,CblasTrans,CblasNonUnit,tB,tB,1,Ajj,tB,Aij,tB);
				t2 = dsecnd();
				tDtrsm += (t2 - t1);	
				nDtrsm++;
				
				t1 = dsecnd();
				asyncWrite(fd,pos,Aij,bytesB, controlW);
				t2 = dsecnd();
				tEsc += (t2 - t1);
				nEsc++;
			}
								
		}	
	}

    fin = dsecnd();
	
	printf("Cholesky ooC time: %g \n",fin-ini);
	printf("Tiempo lectura %g\n",tLect);
	printf("Tiempo Escritura %g\n",tEsc);
	printf("Tiempo Dgemm %g\n",tDgemm);
	printf("Tiempo cholesky %g\n",tChol);
	printf("Tiempo dtrsm %g\n",tDtrsm);
	printf("Tiempo cambios formato %g\n",tForm);
	printf("Tiempo resta %g\n",tResta);
	

	printf("num lecturas %d\n",nLect);
	printf("num Escritura %d\n",nEsc);
	printf("num Dgemm %d\n",nDgemm);
	printf("num cholesky %d\n",nChol);
	printf("num dtrsm %d\n",nDtrsm);
	printf("num cambios formato %d\n",nForm);
	printf("num restas %d\n",nResta);
	
	return 0;
}

/**
 *  \fn   void asyncWrite(const int fd, const unsigned long iniPos, double* buff, const unsigned long nBytes, struct aiocb* control)
 *  \brief This function performs an asynchronous write opperation using the AIO POSIX interface, performing multiple operations when 
 *          the bytes to write are bigger than the maximum data suported by the AIO functions.
 *  \param fd: (input) File descriptor number
 *  \param iniPos: (input) Position  to write in the file
 *  \param buff: (input) Buffer with the data to write in the file
 *  \param nBytes: (input) Number of bytes to write
 *  \param control: (input) Array of aiocb structures to handle the writings
*/
void asyncWrite(const int fd, const unsigned long iniPos, double* buff, const unsigned long nBytes, struct aiocb* control)
{
	int i;
	unsigned long wBlocks, pos;
	
	pos = iniPos;
	wBlocks = nBytes / TRANSFER_MAX;
	
	for(i= 0; i < wBlocks; i++)
        {
                control[i].aio_fildes = fd;
                control[i].aio_offset=pos;
                control[i].aio_buf = &buff[i* ELEM_TRMAX];
                control[i].aio_nbytes = TRANSFER_MAX;
                aio_write(&control[i]);
                pos = pos + TRANSFER_MAX;

        }

        control[i].aio_fildes = fd;
        control[i].aio_offset = pos;
        control[i].aio_buf = &buff[i* ELEM_TRMAX];
        control[i].aio_nbytes = nBytes % TRANSFER_MAX;
        aio_write(&control[i]);
}
