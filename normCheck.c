#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <mkl.h>

/* Matrices stored by columns: BLAS style */
#define	A(i,j)		A[ (i) + ((j)*(n)) ]
#define	G(i,j)		G[ (i) + ((j)*(n)) ]
#define	Gb(i,j)		Gb[ (i) + ((j)*(n)) ]
#define	G2(i,j)		G2[ (i) + ((j)*(n)) ]
#define	M(i,j)		M[ (i) + ((j)*(n)) ]
#define	L(i,j)		L[ (i) + ((j)*(n)) ]

double checkNorm(double* A, double* G, long n);
void extraeTriInf(double* M, double* L,long n,long ldm, long ldl);
void printMatrix(double* M, long n);



int main(int argc, const char* argv[])
{
	int i, j,x,y;
	int TB;
	unsigned long n,sbTam,nB;
	FILE* f;
	double ini,fin,t;
	const char* fname,*fnameChol;
	double* B,*Aij, *L, *Lc;
	
	
	if(argc != 3)
	{
		printf("Uso: cholesky problem_file cholesky_file");
		exit(1);
	}
	else
	{
		fname = argv[1];
		fnameChol = argv[2];
	}
	
	/******** reserva de memoria para las matrices y creación de A*******/
	double * A;


	printf("Reading problem...\n");
    ini = dsecnd();
	f = fopen(fname,"rb+");
	fread(&n,sizeof(unsigned long),1,f);
	fread(&sbTam,sizeof(unsigned long),1,f);
	A = (double *) calloc( n*n,sizeof(double) );
	if(A == NULL){
		printf("Error en la reserva de memoria de A\n");
	}
	nB = n/sbTam;
	for(j = 0; j < nB; j++)
		for(i = j; i < nB; i++ )
		{
			if(i == j)
			{
				B = (double *) malloc( sbTam*(sbTam+1)/2*sizeof(double) );
				fread(B,sizeof(double),sbTam*(sbTam+1)/2,f);
				LAPACKE_dtfttr(LAPACK_COL_MAJOR,'N','L',sbTam,B,&A[i*sbTam+j*sbTam*n],n);
				
				if(i == 0)
				{
					L = (double *) malloc( sbTam*sbTam*sizeof(double));
					LAPACKE_dtfttr(LAPACK_COL_MAJOR,'N','L',sbTam,B,L,sbTam);
					for(y = 0; y < sbTam; y++)
						for(x = y+1; x < sbTam; x++)
							L[y+x*sbTam]=L[x+y*sbTam];
				}
				free(B);
				
			}else
			{
				B = (double *) malloc( sbTam*sbTam*sizeof(double));
				fread(B,sizeof(double),sbTam*sbTam,f);
				Aij = &A[i*sbTam+j*sbTam*n];
				for(x = 0; x < sbTam; x++)
					for(y = 0; y < sbTam; y++)
						Aij[x+y*n] = B[x+y*sbTam] ;
				free(B);
			}
		}
	
	//Chapuza temporal
	for(j = 0; j < n; j++)
		for(i = j+1; i < n; i++)
			A[j+i*n]=A[i+j*n];
	
	fclose(f);

	fin = dsecnd();
	printf("Read time: %g \n",fin-ini);
	
	double * Ac;


	printf("Reading cholesky...\n");
    ini = dsecnd();
	f = fopen(fnameChol,"rb+");
	fread(&n,sizeof(unsigned long),1,f);
	fread(&sbTam,sizeof(unsigned long),1,f);
	Ac = (double *) calloc( n*n,sizeof(double) );
	if(Ac == NULL){
		printf("Error en la reserva de memoria de A\n");
	}
	nB = n/sbTam;
	for(j = 0; j < nB; j++)
		for(i = j; i < nB; i++ )
		{
			if(i == j)
			{
				B = (double *) malloc( sbTam*(sbTam+1)/2*sizeof(double) );
				fread(B,sizeof(double),sbTam*(sbTam+1)/2,f);
				LAPACKE_dtfttr(LAPACK_COL_MAJOR,'N','L',sbTam,B,&Ac[i*sbTam+j*sbTam*n],n);
				
				if(i == 0)
				{
					Lc = (double *) malloc( sbTam*sbTam*sizeof(double));
					LAPACKE_dtfttr(LAPACK_COL_MAJOR,'N','L',sbTam,B,Lc,sbTam);
					for(y = 0; y < sbTam; y++)
						for(x = y+1; x < sbTam; x++)
							L[y+x*sbTam]=L[x+y*sbTam];
				}
				free(B);
				
				
			}else
			{
				B = (double *) malloc( sbTam*sbTam*sizeof(double));
				fread(B,sizeof(double),sbTam*sbTam,f);
				Aij = &Ac[i*sbTam+j*sbTam*n];
				for(x = 0; x < sbTam; x++)
					for(y = 0; y < sbTam; y++)
						Aij[x+y*n] = B[x+y*sbTam] ;
				free(B);
			}
		}
	
	//Chapuza temporal
	for(j = 0; j < n; j++)
		for(i = j+1; i < n; i++)
			Ac[j+i*n]=Ac[i+j*n];
	
	fclose(f);

	fin = dsecnd();
	printf("Read time: %g \n",fin-ini);

	printf("Norma A-LL'  = %.3e\n",checkNorm(A,Ac,n));
	printf("Norma A-LL' L  = %.3e\n",checkNorm(L,Lc,sbTam));
	

		
	return 0;
}

// Extrae la triangular inferior de G, calcula GG', resta A-GG' y devuelve la 2norma de la matriz resultante.
double checkNorm(double* A, double* G, long n)
{
	double* L =  (double*)calloc(n * n,sizeof(double));
	double* M =  (double*)calloc(n * n,sizeof(double));
	long i,j;
	double norm;
	
	extraeTriInf(G,L,n,n,n);
	
	cblas_dgemm (CblasColMajor, CblasNoTrans, CblasTrans, n, n, n, 1, L, n, L, n, 0, M, n);
	
	for(i=0;i<n;i++)
		for(j=0;j<n;j++)
			M(i,j)=M(i,j)-A(i,j);
	
	norm = cblas_dnrm2(n*n,M,1);
	//printMatrix(M,n);
	
	free(L);free(M);
	
	return norm;
	
}

void printMatrix(double* M, long n)
{
    int i,j;
	
	for(i = 0; i < n; i++)
	{
		for(j = 0; j < n; j++)
			printf("%g ",M(i,j));
		printf("\n");
	}
}


void extraeTriInf(double* M, double* L,long n,long ldm, long ldl)
{
	long i,j;
	
	for(i=0;i<n;i++)
    {
		for(j=i+1;j<n;j++)
			L[i+j*ldl]=0.0;
		for(j=0;j<i+1;j++)
			L[i+j*ldl]=M[i+j*ldm];
    }


}
/*_____________________________________________________________________________________________________*/
