#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <mkl.h>
#include <aio.h>


/* Matrices stored by columns: BLAS style */
#define	A(i,j)		A[ (i) + ((j)*(n)) ]
#define	G(i,j)		G[ (i) + ((j)*(n)) ]
#define	Gb(i,j)		Gb[ (i) + ((j)*(n)) ]
#define	G2(i,j)		G2[ (i) + ((j)*(n)) ]
#define	M(i,j)		M[ (i) + ((j)*(n)) ]
#define	L(i,j)		L[ (i) + ((j)*(n)) ]

void printMatrix(double* M, long n);
void restaMatriz(double* M1,double* M2, double* R,long n, long nMem1, long nMem2);


int main(int argc, const char* argv[])
{
	int i, j,k,x,y, info, fd;
	unsigned long n,nB,tB,pos, pos2, bytesB,bytesL,bytesU;
	FILE* f;
	double ini,fin,t;
	const char* fname;
	double* B, *Aij, *Aii,* Aik, *Ajk, *Aij2, *Aik2, *Ajk2, *auxRead, *auxRead2, *Ajj, *S;
	struct aiocb aiocbW, aiocbR, aiocbR2;
	
	aiocbW.aio_sigevent.sigev_notify= SIGEV_NONE;
	aiocbW.aio_reqprio = 0;
	aiocbR.aio_sigevent.sigev_notify= SIGEV_NONE;
	aiocbR.aio_reqprio = 0;
	aiocbR2.aio_sigevent.sigev_notify= SIGEV_NONE;
	aiocbR2.aio_reqprio = 0;
	
	//timers
	int nEsc = 0,nLect = 0,nDgemm = 0, nDtrsm = 0,nChol = 0, nForm = 0, nResta = 0;
	double tEsc = 0,tLect = 0,tDgemm = 0, tDtrsm = 0,tChol = 0, tForm = 0, tResta = 0;
	double t1, t2;
	
	if(argc != 2)
	{
		printf("Uso: cholesky problem_file");
		exit(1);
	}
	else
	{
		fname = argv[1];
	}
	
	/******** reserva de memoria para las matrices y creación de A*******/

	
	
	ini = dsecnd();
	
	f = fopen(fname,"rb+");
	fread(&n,sizeof(unsigned long),1,f);
	fread(&tB,sizeof(unsigned long),1,f);
	fd = fileno(f);
	
	nB = n/ tB;
	bytesB = tB*tB*sizeof(double);
	bytesL = tB*(tB+1)/2*sizeof(double);
	bytesU = tB*(tB-1)/2*sizeof(double);
	printf("n = %d, tB = %d, nB = %d\n",n,tB,nB);
	
	//Memory preallocation
	S = (double*)mkl_malloc(bytesB,64);
	Aik = (double*)mkl_malloc(bytesB,64);
	Ajk = (double*)mkl_malloc(bytesB,64);
	Aik2 = (double*)mkl_malloc(bytesB,64);
	Ajk2 = (double*)mkl_malloc(bytesB,64);
	Aij = Aik;
	Aij2 = Aik2;
	Aii = Aik;
	auxRead = Aik2;
	B = Ajk;
	Ajj = (double*)mkl_malloc(bytesB,64);
	

	//primera columna no necesita nada de columnas anteriores
	j = 0;
	
	t1 = dsecnd();
	fread(Aii,sizeof(double),tB*(tB+1)/2,f);
	t2 = dsecnd();
	tLect += (t2 - t1);
	nLect++;
	
	t1 = dsecnd();
	info = LAPACKE_dpftrf(LAPACK_COL_MAJOR,'N','L',tB,Aii); 
	if (info != 0)
		printf("Error en lapack(dpftrf): %d\n",info);
	t2 = dsecnd();
	tChol += (t2 - t1);
	nChol++;
	
	t1 = dsecnd();
	//fseek(f,16,SEEK_SET);
	//fwrite(Aii,sizeof(double),tB*(tB+1)/2,f);
	aiocbW.aio_fildes = fd;
	aiocbW.aio_offset = 16;
	aiocbW.aio_buf = Aii;
	aiocbW.aio_nbytes = bytesL;
	aio_write(&aiocbW);
	t2 = dsecnd();
	tEsc += (t2 - t1);
	nEsc++;
	
	t1 = dsecnd();
	info = LAPACKE_dtfttr(LAPACK_COL_MAJOR,'N','L',tB,Aii,Ajj,tB);
	if (info != 0)
		printf("Error en lapack(dpftrs): %d\n",info);
	t2 = dsecnd();
	tForm += (t2 - t1);
	nForm++;
	
	t1 = dsecnd();
	i = 1;
	pos = (i+j*nB)*bytesB-(j+1)*bytesU+16;
	fseek(f,pos,SEEK_SET);
	fread(Aij,sizeof(double),tB*tB,f);
	t2 = dsecnd();
	tLect += (t2 - t1);
	nLect++;
	
	for(i = 2; i < nB; i++)
	{
		//printf("[%d,%d]pos: %d\n",i,j,ftell(f))
		t1 = dsecnd();
		//printf("[%d,%d]pos: %d\n",i,j,ftell(f))i
		pos2 = (i+j*nB)*bytesB-(j+1)*bytesU+16;
		aiocbR.aio_fildes = fd;
		aiocbR.aio_offset = pos2;
		aiocbR.aio_buf = Aij2;
		aiocbR.aio_nbytes = bytesB;
		aio_read(&aiocbR);
		t2 = dsecnd();
		tLect += (t2 - t1);
		nLect++;
				
		t1 = dsecnd();
		cblas_dtrsm(CblasColMajor,CblasRight,CblasLower,CblasTrans,CblasNonUnit,tB,tB,1,Ajj,tB,Aij,tB);
		t2 = dsecnd();
		tDtrsm += (t2 - t1);
		nDtrsm++;
		
		t1 = dsecnd();
		//fseek(f,pos,SEEK_SET);
		//fwrite(Aij,sizeof(double),tB*tB,f);
		aiocbW.aio_fildes = fd;
		aiocbW.aio_offset = pos;
		aiocbW.aio_buf = Aij;
		aiocbW.aio_nbytes = bytesB;
		aio_write(&aiocbW);
		t2 = dsecnd();
		tEsc += (t2 - t1);
		nEsc++;
		
		auxRead = Aij;
		Aij = Aij2;
		Aij2 = auxRead;
		pos = pos2;
	}
	
	t1 = dsecnd();
	cblas_dtrsm(CblasColMajor,CblasRight,CblasLower,CblasTrans,CblasNonUnit,tB,tB,1,Ajj,tB,Aij,tB);
	t2 = dsecnd();
	tDtrsm += (t2 - t1);
	nDtrsm++;
	
	t1 = dsecnd();
	//fseek(f,pos,SEEK_SET);
	//fwrite(Aij,sizeof(double),tB*tB,f);
	aiocbW.aio_fildes = fd;
	aiocbW.aio_offset = pos;
	aiocbW.aio_buf = Aij;
	aiocbW.aio_nbytes = bytesB;
	aio_write(&aiocbW);
	t2 = dsecnd();
	tEsc += (t2 - t1);
	nEsc++;
		
	//resto de columnas
	for(j = 1; j < nB; j++)
	{
		for(i = j; i < nB; i++)
		{
			if( i == j)
			{
				cblas_dscal(tB*tB,0,S,1);//init S to 0 
				
				t1 = dsecnd();
				fseek(f,i*bytesB-bytesU+16,SEEK_SET);
				fread(Aik,sizeof(double),tB*tB,f);
				t2 = dsecnd();
				tLect += (t2 - t1);
				nLect++;
				
				for(k = 1; k < j;k++)
				{
					t1 = dsecnd();
					aiocbR.aio_fildes = fd;
					aiocbR.aio_offset = (i+k*nB-(k*(k+1)/2))*bytesB-(k+1)*bytesU+16;
					aiocbR.aio_buf = Aik2;
					aiocbR.aio_nbytes = bytesB;
					aio_read(&aiocbR);
					t2 = dsecnd();
					tLect += (t2 - t1);
					nLect++;
					
					t1 = dsecnd();
					cblas_dsyrk(CblasColMajor,CblasLower,CblasNoTrans,tB,tB,1,Aik,tB,1,S,tB);
					t2 = dsecnd();
					tDgemm += (t2 - t1);
					nDgemm++;
					
					auxRead = Aik;
					Aik = Aik2; 
					Aik2 = auxRead;
				}
				pos = (i+j*nB-(j*(j+1)/2))*bytesB-(j)*bytesU+16;
				
				t1 = dsecnd();
				Aii = auxRead;
				aiocbR.aio_fildes = fd;
				aiocbR.aio_offset = pos;
				aiocbR.aio_buf = Aii;
				aiocbR.aio_nbytes = bytesL;
				aio_read(&aiocbR);
				t2 = dsecnd();
				tLect += (t2 - t1);
				nLect++;
				
				t1 = dsecnd();
				cblas_dsyrk(CblasColMajor,CblasLower,CblasNoTrans,tB,tB,1,Aik,tB,1,S,tB);
				t2 = dsecnd();
				tDgemm += (t2 - t1);
				nDgemm++;
				
				t1 = dsecnd();
				//resta Aii = Aii - S
				LAPACKE_dtrttf(LAPACK_COL_MAJOR,'N','L',tB,S,tB,B);
				t2 = dsecnd();
				tForm += (t2 - t1);
				nForm++;
				
				t1 = dsecnd();
				cblas_daxpy(tB*(tB+1)/2,-1,B,1,Aii,1);
				t2 = dsecnd();
				tResta += (t2 - t1);
				nResta++;
				
				t1 = dsecnd();
				info = LAPACKE_dpftrf(LAPACK_COL_MAJOR,'N','L',tB,Aii); 
				if (info != 0)
					printf("[%d,%d]Error en lapack(dpftrf): %d\n",i,j,info);
				t2 = dsecnd();
				tChol += (t2 - t1);
				nChol++;
				
				t1 = dsecnd();
				//fseek(f,pos,SEEK_SET);
				//fwrite(Aii,sizeof(double),tB*(tB+1)/2,f);
				aiocbW.aio_fildes = fd;
				aiocbW.aio_offset = pos;
				aiocbW.aio_buf = Aii;
				aiocbW.aio_nbytes = bytesL;
				aio_write(&aiocbW);
				t2 = dsecnd();
				tEsc += (t2 - t1);
				nEsc++;
				
				t1 = dsecnd();
				info = LAPACKE_dtfttr(LAPACK_COL_MAJOR,'N','L',tB,Aii,Ajj,tB);
				t2 = dsecnd();
				tForm += (t2 - t1);
				nForm++;
			}
			else
			{
				cblas_dscal(tB*tB,0,S,1);//init S to 0 
				
				t1 = dsecnd();
				fseek(f,i*bytesB-bytesU+16,SEEK_SET);
				fread(Aik,sizeof(double),tB*tB,f);
					
				fseek(f,j*bytesB-bytesU+16,SEEK_SET);
				fread(Ajk,sizeof(double),tB*tB,f);
				t2 = dsecnd();
				tLect += (t2 - t1);
				nLect++;
				
				for(k = 1; k < j;k++)
				{
					t1 = dsecnd();
					aiocbR.aio_fildes = fd;
					aiocbR.aio_offset = (i+k*nB-(k*(k+1)/2))*bytesB-(k+1)*bytesU+16;
					aiocbR.aio_buf = Aik2;
					aiocbR.aio_nbytes = bytesB;
					aio_read(&aiocbR);
					
					aiocbR2.aio_fildes = fd;
					aiocbR2.aio_offset = (j+k*nB-(k*(k+1)/2))*bytesB-(k+1)*bytesU+16;
					aiocbR2.aio_buf = Ajk2;
					aiocbR2.aio_nbytes = bytesB;
					aio_read(&aiocbR2);
					t2 = dsecnd();
					tLect += (t2 - t1);
					nLect++;
					
					t1 = dsecnd();
					cblas_dgemm(CblasColMajor,CblasNoTrans,CblasTrans,tB,tB,tB,1,Aik,tB,Ajk,tB,1,S,tB);	
					t2 = dsecnd();
					tDgemm += (t2 - t1);
					nDgemm++;
					
					auxRead = Aik; auxRead2 = Ajk;
					Aik = Aik2; Ajk = Ajk2;
					Aik2 = auxRead; Ajk2 = auxRead2;
					
				}
				
				pos = (i+j*nB-(j*(j+1)/2))*bytesB-(j+1)*bytesU+16;
				t1 = dsecnd();
				Aij = auxRead;
				aiocbR.aio_fildes = fd;
				aiocbR.aio_offset = pos;
				aiocbR.aio_buf = Aij;
				aiocbR.aio_nbytes = bytesB;
				aio_read(&aiocbR);
				t2 = dsecnd();
				tLect += (t2 - t1);
				nLect++;
				
				t1 = dsecnd();
				cblas_dgemm(CblasColMajor,CblasNoTrans,CblasTrans,tB,tB,tB,1,Aik,tB,Ajk,tB,1,S,tB);	
				t2 = dsecnd();
				tDgemm += (t2 - t1);
				nDgemm++;
				
				t1 = dsecnd();
				cblas_daxpy(tB*tB,-1,S,1,Aij,1);
				t2 = dsecnd();
				tResta += (t2 - t1);
				nResta++;
				
				t1 = dsecnd();
				cblas_dtrsm(CblasColMajor,CblasRight,CblasLower,CblasTrans,CblasNonUnit,tB,tB,1,Ajj,tB,Aij,tB);
				t2 = dsecnd();
				tDtrsm += (t2 - t1);	
				nDtrsm++;
				
				t1 = dsecnd();
				//fseek(f,pos,SEEK_SET);
				//fwrite(Aij,sizeof(double),tB   *tB,f);
				aiocbW.aio_fildes = fd;
				aiocbW.aio_offset = pos;
				aiocbW.aio_buf = Aij;
				aiocbW.aio_nbytes = bytesB;
				aio_write(&aiocbW);
				t2 = dsecnd();
				tEsc += (t2 - t1);
				nEsc++;
			}
								
		}	
	}

    fin = dsecnd();
	
	printf("Cholesky ooC time: %g \n",fin-ini);
		printf("Tiempo lectura %g\n",tLect);
	printf("Tiempo Escritura %g\n",tEsc);
	printf("Tiempo Dgemm %g\n",tDgemm);
	printf("Tiempo cholesky %g\n",tChol);
	printf("Tiempo dtrsm %g\n",tDtrsm);
	printf("Tiempo cambios formato %g\n",tForm);
	printf("Tiempo resta %g\n",tResta);
	

	printf("num lecturas %d\n",nLect);
	printf("num Escritura %d\n",nEsc);
	printf("num Dgemm %d\n",nDgemm);
	printf("num cholesky %d\n",nChol);
	printf("num dtrsm %d\n",nDtrsm);
	printf("num cambios formato %d\n",nForm);
	printf("num restas %d\n",nResta);
		
	return 0;
}

void printMatrix(double* M, long n)
{
    int i,j;
	
	for(i = 0; i < n; i++)
	{
		for(j = 0; j < n; j++)
			printf("%g ",M(i,j));
		printf("\n");
	}
}

/**
*Resta dos matrices cuadradas del mismo tamaño
*@param M1 puntero a matriz o bloque
*@param M2 puntero a segundo operando matriz o bloque
*@param tamaño del bloque
*@param nMem1 tamaño en memoria de M1
*@param nMem2 tamaño en memoria de M2
*/
void restaMatriz(double* M1,double* M2, double* R,long n, long nMem1, long nMem2)
{
	//double* R = (double*)mkl_malloc(n * n * sizeof(double),64);
	
	int i,j;
	for(i = 0; i < n; i++)
		for(j = 0; j < n; j++)
			R[i+j*n] = M1[i+j*nMem1] - M2[i+j*nMem2];
			
	//return R;
}
