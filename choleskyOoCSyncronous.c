#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#define MKL_ILP64
#include <mkl.h>
#include <aio.h>


int main(int argc, const char* argv[])
{
	int i, j,k,x,y, info, fd;
	size_t info2;
	unsigned long n,nB,tB,pos, bytesB,bytesL,bytesU;
	FILE* f;
	double ini,fin,t;
	const char* fname;
	double* B,*Aij,* Aii,* Aik, *Ajk,* Ajj,* S;
	struct aiocb aioConf;
	
	aioConf.aio_sigevent.sigev_notify= SIGEV_NONE;
	aioConf.aio_reqprio = 0;
	
	
	if(argc != 2)
	{
		printf("Uso: cholesky problem_file");
		exit(1);
	}
	else
	{
		fname = argv[1];
	}
	
	ini = dsecnd();
	//Parameter reading	
	f = fopen(fname,"rb+");
	fread(&n,sizeof(unsigned long),1,f);
	fread(&tB,sizeof(unsigned long),1,f);
	fd = fileno(f);
	
	nB = n/ tB;
	bytesB = tB*tB*sizeof(double);
	bytesL = tB*(tB+1)/2*sizeof(double);
	bytesU = tB*(tB-1)/2*sizeof(double);

	//Memory preallocation
	S = (double*)mkl_malloc(bytesB,64);
	Aik = (double*)mkl_malloc(bytesB,64);
	Ajk = (double*)mkl_malloc(bytesB,64);
	Aij = Aik;
	Aii = Aik;
	B = Ajk;
	Ajj = (double*)mkl_malloc(bytesB,64);
	

	//First column doesn't need anything from previous columns
	j = 0;
	
	fread(Aii,sizeof(double),tB*(tB+1)/2,f);
	
	info = LAPACKE_dpftrf(LAPACK_COL_MAJOR,'N','L',tB,Aii); 
	if (info != 0)
		printf("Error en lapack(dpftrf): %d\n",info);
	
	
	fseek(f,16,SEEK_SET);
	fwrite(Aii,sizeof(double),tB*(tB+1)/2,f);
	
	info = LAPACKE_dtfttr(LAPACK_COL_MAJOR,'N','L',tB,Aii,Ajj,tB);
	if (info != 0)
		printf("Error en lapack(dpftrs): %d\n",info);
	
	for(i = 1; i < nB; i++)
	{
		pos = (i+j*nB)*bytesB-(j+1)*bytesU+16;
		fseek(f,pos,SEEK_SET);
		fread(Aij,sizeof(double),tB*tB,f);
				
		cblas_dtrsm(CblasColMajor,CblasRight,CblasLower,CblasTrans,CblasNonUnit,tB,tB,1,Ajj,tB,Aij,tB);
		
		fseek(f,pos,SEEK_SET);
		fwrite(Aij,sizeof(double),tB*tB,f);

	}
		
	//remaining columns
	for(j = 1; j < nB; j++)
	{
		for(i = j; i < nB; i++)
		{
			if( i == j)
			{
			
				cblas_dscal(tB*tB,0,S,1);//init S to 0 
				for(k = 0; k < j;k++)
				{
					fseek(f,(i+k*nB-(k*(k+1)/2))*bytesB-(k+1)*bytesU+16,SEEK_SET);
					fread(Aik,sizeof(double),tB*tB,f);
							   
					cblas_dsyrk(CblasColMajor,CblasLower,CblasNoTrans,tB,tB,1,Aik,tB,1,S,tB);
				}
				pos = (i+j*nB-(j*(j+1)/2))*bytesB-(j)*bytesU+16;
				fseek(f,pos,SEEK_SET);
				
				fread(Aii,sizeof(double),tB*(tB+1)/2,f);

				//Convert S to RFP format
				info = LAPACKE_dtrttf(LAPACK_COL_MAJOR,'N','L',tB,S,tB,B);
				//resta Aii = Aii - S 
				cblas_daxpy(tB*(tB+1)/2,-1,B,1,Aii,1);
				
				//Cholesky decomposition
				info = LAPACKE_dpftrf(LAPACK_COL_MAJOR,'N','L',tB,Aii); 
				if (info != 0)
					printf("[%d,%d]Error en lapack(dpftrf): %d\n",i,j,info);
				
				fseek(f,pos,SEEK_SET);
				fwrite(Aii,sizeof(double),tB*(tB+1)/2,f);
				
				//Unpacking Aii from RFP storage to full storage
				info2 = LAPACKE_dtfttr(LAPACK_COL_MAJOR,'N','L',tB,Aii,Ajj,tB);
				if (info2 != 0)
					printf("[%d,%d]Error on dtfttr: %d\n",i,j,info2);
			}
			else
			{
			
				cblas_dscal(tB*tB,0,S,1);//init S to 0 
				for(k = 0; k < j;k++)
				{
					fseek(f,(i+k*nB-(k*(k+1)/2))*bytesB-(k+1)*bytesU+16,SEEK_SET);
					fread(Aik,sizeof(double),tB*tB,f);
					
					fseek(f,(j+k*nB-(k*(k+1)/2))*bytesB-(k+1)*bytesU+16,SEEK_SET);
					fread(Ajk,sizeof(double),tB*tB,f);
							   
					cblas_dgemm(CblasColMajor,CblasNoTrans,CblasTrans,tB,tB,tB,1,Aik,tB,Ajk,tB,1,S,tB);	
				}
				pos = (i+j*nB-(j*(j+1)/2))*bytesB-(j+1)*bytesU+16;
				fseek(f,pos,SEEK_SET);
				fread(Aij,sizeof(double),tB*tB,f);

				
				cblas_daxpy(tB*tB,-1,S,1,Aij,1);
				
				cblas_dtrsm(CblasColMajor,CblasRight,CblasLower,CblasTrans,CblasNonUnit,tB,tB,1,Ajj,tB,Aij,tB);
					
				fseek(f,pos,SEEK_SET);
				fwrite(Aij,sizeof(double),tB   *tB,f);
			}
								
		}	
	}

    fin = dsecnd();
	
	printf("Cholesky ooC time: %g \n",fin-ini);
		
	return 0;
}

