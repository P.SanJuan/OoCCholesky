Introduction
---------------

This repository contains implementations of the Out of Core (OoC) Cholesky decomposition implemented by P. San Juan and introduced in the paper  referenced in the citation section.


Citation
--------------

When using this code the following article should be cited:

P. San Juan, A.m. Vidal, F.J. Martínez Zaldívar and V. M. Garcia-Molla. Solving large scale positive definite linear systems with memory bound in modern computers. CEDYA + CMA 2017


Dependencies
-----------------
 This code needs the Intel Math Kernel Library installed in the system. 
 https://software.intel.com/en-us/articles/intel-math-kernel-library-documentation
 

Source contents
---------------

- **CholeskyOoCSynchronous.c:** Basic synchronous version of the OoC algorithm.
- **CholeskyOoC.c:** Version of the OoC algorithm that uses asyncronous writings to overlap computations and IO.
- **CholeskyOoC6.c:** Version of the OoC algorithm that uses asyncronous writings and readings to overlap computations and IO. Currently under testing and developent.
- **oocSyncMedidas.c:** Code for measuring the execution time of the different operations within the algorithm.
- **oocMedidas.c:** Code for measuring the execution time of the different operations within the algorithm for the asyncronous writings version.
- **ooc6Medidas.c:** Code for measuring the execution time of the different operations within the algorithm for the full asynchronous IO version.
- **normCheck.c:** Code to check the result of the decomposition. This code reads the original matrix in Out of Core Block format and the result of the Ooc Decomposition to compute the norm of the decomposition.
- **generadorFormula.c:** Formula based generator to generate a symmetric and positive definite matrix in the file format used in the OoC algorithms.
- **generadorProblema.c:** Random matrix based generator to generate a symmetric and positive definite matrix in the file format used in the OoC algorithms.

Compilation example
---------------------

icc -O3 choleskyOoC.c -mkl -lrt -o cholesky 