#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#define MKL_ILP64
#include <mkl.h>
#include <aio.h>


#define TRANSFER_MAX 2147479552 
#define ELEM_TRMAX 268434944

void printMatrix(double* M, long n);
void restaMatriz(double* M1,double* M2, double* R,long n, long nMem1, long nMem2);
void asyncWrite(const int fd, const unsigned long iniPos, double* buff, const unsigned long nBytes, struct aiocb* control );
void asyncRead(const int fd, const unsigned long iniPos, double* buff, const unsigned long nBytes, struct aiocb* control );

int main(int argc, const char* argv[])
{
	int i, j,k,x,y, info, fd, nControl;
	unsigned long n,nB,tB,pos, pos2, bytesB,bytesL,bytesU;
	FILE* f;
	double ini,fin,t;
	const char* fname;
	double* B, *Aij, *Aii,* Aik, *Ajk, *Aij2, *Aik2, *Ajk2, *auxRead, *auxRead2, *Ajj, *S;
	struct aiocb* controlW, *controlR, *controlR2;
	
	
	if(argc != 2)
	{
		printf("Uso: cholesky problem_file");
		exit(1);
	}
	else
	{
		fname = argv[1];
	}
	
	
	ini = dsecnd();
	//Parameter reading	
	f = fopen(fname,"rb+");
	fread(&n,sizeof(unsigned long),1,f);
	fread(&tB,sizeof(unsigned long),1,f);
	fd = fileno(f);
	
	nB = n/ tB;
	bytesB = tB*tB*sizeof(double);
	bytesL = tB*(tB+1)/2*sizeof(double);
	bytesU = tB*(tB-1)/2*sizeof(double);
	printf("n = %d, tB = %d, nB = %d\n",n,tB,nB);

	//Memory preallocation
	S = (double*)mkl_malloc(bytesB,64);
	Aik = (double*)mkl_malloc(bytesB,64);
	Ajk = (double*)mkl_malloc(bytesB,64);
	Aik2 = (double*)mkl_malloc(bytesB,64);
	Ajk2 = (double*)mkl_malloc(bytesB,64);
	Aij = Ajk;
	Aij2 = Aik2;
	Aii = Ajk2;
	B = Ajk;
	Ajj = (double*)mkl_malloc(bytesB,64);
	
	//Async write handlers init
	nControl = 1 +((bytesB - 1)/TRANSFER_MAX);
	controlW = (struct aiocb*)mkl_malloc( nControl * sizeof(struct aiocb),64);
	for(i = 0; i < nControl; i++)
	{
		controlW[i].aio_sigevent.sigev_notify= SIGEV_NONE;
		controlW[i].aio_reqprio = 0;
	}
	
	//Async read handlers init
	controlR = (struct aiocb*)mkl_malloc( nControl * sizeof(struct aiocb),64);
	for(i = 0; i < nControl; i++)
	{
		controlR[i].aio_sigevent.sigev_notify= SIGEV_NONE;
		controlR[i].aio_reqprio = 0;
	}
	controlR2 = (struct aiocb*)mkl_malloc( nControl * sizeof(struct aiocb),64);
	for(i = 0; i < nControl; i++)
	{
		controlR2[i].aio_sigevent.sigev_notify= SIGEV_NONE;
		controlR2[i].aio_reqprio = 0;
	}
	
	
	//First column doesn't need anything from previous columns
	fseek(f,16,SEEK_SET);	
	fread(Aii,sizeof(double),tB*(tB+1)/2,f);
	
	info = LAPACKE_dpftrf(LAPACK_COL_MAJOR,'N','L',tB,Aii); 
	if (info != 0)
		printf("Error en lapack(dpftrf): %d\n",info);
	
	asyncWrite(fd,16,Aii,bytesL, controlW);
	
	info = LAPACKE_dtfttr(LAPACK_COL_MAJOR,'N','L',tB,Aii,Ajj,tB);
	if (info != 0)
		printf("Error en lapack(dpftrs): %d\n",info);
	i = 1;
	
	pos = i*bytesB-bytesU+16;
	fseek(f,pos,SEEK_SET);
	fread(Aij,sizeof(double),tB*tB,f);
	
	for(i = 2; i < nB; i++)
	{
		pos2 = i*bytesB-bytesU+16;
		asyncRead(fd,pos2,Aij2,bytesB, controlR);
				
		cblas_dtrsm(CblasColMajor,CblasRight,CblasLower,CblasTrans,CblasNonUnit,tB,tB,1,Ajj,tB,Aij,tB);
		
		asyncWrite(fd,pos,Aij,bytesB, controlW);
		
		auxRead = Aij;
		Aij = Aij2;
		Aij2 = auxRead;
		pos = pos2;
	}
	
	cblas_dtrsm(CblasColMajor,CblasRight,CblasLower,CblasTrans,CblasNonUnit,tB,tB,1,Ajj,tB,Aij,tB);
	
	asyncWrite(fd,pos,Aij,bytesB, controlW);
	
	
	//remaining columns
	for(j = 1; j < nB; j++)
	{
		for(i = j; i < nB; i++)
		{
			if( i == j)
			{
				cblas_dscal(tB*tB,0,S,1);//init S to 0 
				
				fseek(f,i*bytesB-bytesU+16,SEEK_SET);
				fread(Aik,sizeof(double),tB*tB,f);
				
				for(k = 1; k < j;k++)
				{
					pos = (i+k*nB-(k*(k+1)/2))*bytesB-(k+1)*bytesU+16;
					asyncRead(fd,pos,Aik2,bytesB, controlR);

					cblas_dsyrk(CblasColMajor,CblasLower,CblasNoTrans,tB,tB,1,Aik,tB,1,S,tB);
					auxRead = Aik;
					Aik = Aik2; 
					Aik2 = auxRead;
				}
				pos = (i+j*nB-(j*(j+1)/2))*bytesB-(j)*bytesU+16;
				
				asyncRead(fd,pos,Aii,bytesL, controlR);
				
				cblas_dsyrk(CblasColMajor,CblasLower,CblasNoTrans,tB,tB,1,Aik,tB,1,S,tB);
				
				//substraction Aii = Aii - S packing S to RFPStorage
				LAPACKE_dtrttf(LAPACK_COL_MAJOR,'N','L',tB,S,tB,B);
				cblas_daxpy(tB*(tB+1)/2,-1,B,1,Aii,1);

				
				info = LAPACKE_dpftrf(LAPACK_COL_MAJOR,'N','L',tB,Aii); 
				if (info != 0)
					printf("[%d,%d]Error en lapack(dpftrf): %d\n",i,j,info);
				
				asyncWrite(fd,pos,Aii,bytesL, controlW);
				
				//Unpacking Aii from RFP storage to full storage
				info = LAPACKE_dtfttr(LAPACK_COL_MAJOR,'N','L',tB,Aii,Ajj,tB);
			}
			else
			{
				cblas_dscal(tB*tB,0,S,1);//init S to 0 
				
				fseek(f,i*bytesB-bytesU+16,SEEK_SET);
				fread(Aik,sizeof(double),tB*tB,f);
					
				fseek(f,j*bytesB-bytesU+16,SEEK_SET);
				fread(Ajk,sizeof(double),tB*tB,f);
				
				for(k = 1; k < j;k++)
				{
					pos = (i+k*nB-(k*(k+1)/2))*bytesB-(k+1)*bytesU+16;
					asyncRead(fd,pos,Aik2,bytesB, controlR);
					
					pos2 = (j+k*nB-(k*(k+1)/2))*bytesB-(k+1)*bytesU+16;
					asyncRead(fd,pos2,Ajk2,bytesB, controlR2);
					
					cblas_dgemm(CblasColMajor,CblasNoTrans,CblasTrans,tB,tB,tB,1,Aik,tB,Ajk,tB,1,S,tB);	
					auxRead = Aik; auxRead2 = Ajk;
					Aik = Aik2; Ajk = Ajk2;
					Aik2 = auxRead; Ajk2 = auxRead2;
					
				}
				
				pos = (i+j*nB-(j*(j+1)/2))*bytesB-(j+1)*bytesU+16;
				Aij = Aik2;
				asyncRead(fd,pos,Aij,bytesB, controlR);
				
				cblas_dgemm(CblasColMajor,CblasNoTrans,CblasTrans,tB,tB,tB,1,Aik,tB,Ajk,tB,1,S,tB);	
				
				cblas_daxpy(tB*tB,-1,S,1,Aij,1);
				
				cblas_dtrsm(CblasColMajor,CblasRight,CblasLower,CblasTrans,CblasNonUnit,tB,tB,1,Ajj,tB,Aij,tB);
					
				asyncWrite(fd,pos,Aij,bytesB, controlW);
			}
								
		}	
	}

    fin = dsecnd();
	
	printf("Cholesky ooC time: %g \n",fin-ini);
		
	return 0;
}

/**
 *  \fn   void asyncWrite(const int fd, const unsigned long iniPos, double* buff, const unsigned long nBytes, struct aiocb* control)
 *  \brief This function performs an asynchronous write opperation using the AIO POSIX interface, performing multiple operations when 
 *          the bytes to write are bigger than the maximum data suported by the AIO functions.
 *  \param fd: (input) File descriptor number
 *  \param iniPos: (input) Position  to write in the file
 *  \param buff: (input) Buffer with the data to write in the file
 *  \param nBytes: (input) Number of bytes to write
 *  \param control: (input) Array of aiocb structures to handle the writings
*/
void asyncWrite(const int fd, const unsigned long iniPos, double* buff, const unsigned long nBytes, struct aiocb* control )
{
	int i;
	unsigned long wBlocks, pos;
	
	pos = iniPos;
	wBlocks = nBytes / TRANSFER_MAX;
	
	for(i= 0; i < wBlocks; i++)
	{
		control[i].aio_fildes = fd;
		control[i].aio_offset=pos;
		control[i].aio_buf = &buff[i* ELEM_TRMAX];
		control[i].aio_nbytes = TRANSFER_MAX;
		aio_write(&control[i]);
		pos = pos + TRANSFER_MAX;
		
	}
	
	control[i].aio_fildes = fd;
	control[i].aio_offset=pos;
	control[i].aio_buf = &buff[i* ELEM_TRMAX];
	control[i].aio_nbytes = nBytes % TRANSFER_MAX;
	aio_write(&control[i]);
}

/**
 *  \fn   void asyncRead(const int fd, const unsigned long iniPos, double* buff, const unsigned long nBytes, struct aiocb* control )
 *  \brief This function performs an asynchronous read opperation using the AIO POSIX interface, performing multiple operations when 
 *          the bytes to write are bigger than the maximum data suported by the AIO functions.
 *  \param fd: (input) File descriptor number
 *  \param iniPos: (input) Position  to write in the file
 *  \param buff: (input) Buffer with the data to write in the file
 *  \param nBytes: (input) Number of bytes to write
 *  \param control: (input) Array of aiocb structures to handle the writings
*/
void asyncRead(const int fd, const unsigned long iniPos, double* buff, const unsigned long nBytes, struct aiocb* control )
{
	int i;
	unsigned long rBlocks, pos;
	
	pos = iniPos;
	rBlocks = nBytes / TRANSFER_MAX;
	
	for(i= 0; i < rBlocks; i++)
	{
		control[i].aio_fildes = fd;
		control[i].aio_offset=pos;
		control[i].aio_buf = &buff[i* ELEM_TRMAX];
		control[i].aio_nbytes = TRANSFER_MAX;
		aio_read(&control[i]);
		pos = pos + TRANSFER_MAX;
		
	}
	
	control[i].aio_fildes = fd;
	control[i].aio_offset=pos;
	control[i].aio_buf = &buff[i* ELEM_TRMAX];
	control[i].aio_nbytes = nBytes % TRANSFER_MAX;
	aio_read(&control[i]);
}
