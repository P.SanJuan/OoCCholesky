#include <stdio.h>
#include <stdlib.h>
#include <mkl.h>
#include <time.h>

void createCholMatrix(double* A, unsigned long n);
void printMatrix(double* M, long n);

int main(int argc, const char* argv[])
{
	unsigned long n, sbTam,nB,i,j,k,x,y;
	unsigned int count = 0;
	double ini,fin;
	const char* fname;
	double * B, * Bc, * Bt, * Aij;
	FILE* f;
	
	if(argc != 4)
	{
		printf("Uso: generadorP tama�o_matriz tama�o_superbloque nomFich");
		exit(1);
	}
	else
	{
		n = atol(argv[1]);
		sbTam = atol(argv[2]);
		nB = n/ sbTam;
		printf("Tam = %d sbTam= %d nB= %d\n",n,sbTam,nB);
		fname = argv[3];
	}
	srand(time(0));
	printf("Creating matrix...\n");
	
	
	//printMatrix(A,n);
	/****** Escritura por bloques en fichero *******/
	
ini = dsecnd();
	f = fopen(fname,"wb");
	fwrite(&n,sizeof(unsigned long),1,f);
	fwrite(&sbTam,sizeof(unsigned long),1,f);
	
	Bc = (double *) calloc( sbTam*sbTam,sizeof(double));
	
	for(y = 0; y < sbTam; y++)
		for(x = 0; x < sbTam; x++)
			Bc[x+y*sbTam] = rand() % 100 + 1;

	for(j = 0; j < nB; j++)
	{
		
		for(i = j; i < nB; i++ )
		{

			Aij = (double *) calloc( sbTam*sbTam,sizeof(double) );
			
			if (i == j)
			{
				Bt = (double *) calloc( sbTam*sbTam,sizeof(double));
				for(y = 0; y < sbTam; y++)
				{
					Bt[y+y*sbTam] = 1000;	
					for(x = y+1; x < sbTam; x++)
						Bt[x+y*sbTam] = rand() % 100 + 1;
				}
			}
			
			for(k=0; k < j; k++)
			{
			
				cblas_dgemm(CblasColMajor,CblasNoTrans,CblasTrans,sbTam,sbTam,sbTam,1,Bc,sbTam,Bc,sbTam,1,Aij,sbTam);
			}
			
			if(i == j)
			{
				cblas_dgemm(CblasColMajor,CblasNoTrans,CblasTrans,sbTam,sbTam,sbTam,1,Bt,sbTam,Bt,sbTam,1,Aij,sbTam);
				B = (double *) malloc( sbTam*(sbTam+1)/2*sizeof(double) );
				LAPACKE_dtrttf(LAPACK_COL_MAJOR,'N','L',sbTam,Aij,sbTam,B);
				fwrite(B,sizeof(double),sbTam*(sbTam+1)/2,f);
				free(B);
			}
			else
			{
				cblas_dgemm(CblasColMajor,CblasNoTrans,CblasTrans,sbTam,sbTam,sbTam,1,Bc,sbTam,Bt,sbTam,1,Aij,sbTam);
				fwrite(Aij,sizeof(double),sbTam*sbTam,f);
			}
				
			free(Aij);
		}
		free(Bt);
	}
	fclose(f);

	fin = dsecnd();
	printf("Creation time: %g \n",fin-ini);
	
	return 0;
}

//Auxiliar function to create a positive definite matrix
void createCholMatrix(double* A, unsigned long n)
{
	long i,j;
	double* G =  (double*)calloc(n * n,sizeof(double));
	
	for(j = 0; j < n; j++)
	{
		G[j+j*n] = 1000;
		for(i = j+1; i < n; i++)
			G[i+j*n] = rand() % 100 + 1;
	}
			
	cblas_dgemm(CblasColMajor,CblasTrans,CblasNoTrans,n,n,n,1,G,n,G,n,0,A,n);
	//cblas_dtrmm(CblasColMajor,CblasRight,CblasLower,CblasTrans,CblasNonUnit,n,n,1,G,n,G,n);
	free(G);
}

void printMatrix(double* M, long n)
{
    int i,j;
	
	for(i = 0; i < n; i++)
	{
		for(j = 0; j < n; j++)
			printf("%g ",M[i+j*n]);
		printf("\n");
	}
}