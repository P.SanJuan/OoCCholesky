#include <stdio.h>
#include <stdlib.h>
#include <mkl.h>
#include <time.h>

void createCholMatrix(double* A, unsigned long n);
void printMatrix(double* M, long n);

int main(int argc, const char* argv[])
{
	unsigned long n, sbTam,nB,i,j,k,x,y,ig,jg;
	unsigned int count = 0;
	double ini,fin;
	const char* fname;
	double * B, * Aij;
	FILE* f;
	
	if(argc != 4)
	{
		printf("Uso: generadorF tamaño_matriz tamaño_superbloque nomFich");
		exit(1);
	}
	else
	{
		n = atol(argv[1]);
		sbTam = atol(argv[2]);
		nB = n/ sbTam;
		printf("Tam = %d sbTam= %d nB= %d\n",n,sbTam,nB);
		fname = argv[3];
	}
	srand(time(0));
	printf("Creating matrix using formula...\n");
	
	

	/****** Escritura por bloques en fichero *******/
	
ini = dsecnd();
	f = fopen(fname,"wb");
	fwrite(&n,sizeof(unsigned long),1,f);
	fwrite(&sbTam,sizeof(unsigned long),1,f);
	
	B = (double *) malloc( sbTam*(sbTam+1)/2*sizeof(double) );
		
	for(j = 0; j < nB; j++)
	{
		
		for(i = j; i < nB; i++ )
		{

			Aij = (double *) calloc( sbTam*sbTam,sizeof(double) );
			
			for(x=0,ig =i*sbTam ; x < sbTam; x++,ig++)
			{
				
				for(y = 0,jg=j*sbTam; y < sbTam; y++, jg++)
					Aij[x+y*sbTam] = (1+ig+jg)/(1+(ig-jg)*(ig-jg));
				
			}
			
			if(i == j)
			{
				LAPACKE_dtrttf(LAPACK_COL_MAJOR,'N','L',sbTam,Aij,sbTam,B);
				fwrite(B,sizeof(double),sbTam*(sbTam+1)/2,f);
			}
			else
				fwrite(Aij,sizeof(double),sbTam*sbTam,f);
				
			free(Aij);
		}
		
	}
	fclose(f);

	fin = dsecnd();
	printf("Creation time: %g \n",fin-ini);
	
	free(B);
	return 0;
}
